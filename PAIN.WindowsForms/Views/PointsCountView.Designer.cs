﻿namespace PAIN.WindowsForms
{
    partial class PointsCountView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PointsCountView));
            this.label1 = new System.Windows.Forms.Label();
            this.NumberOfPoints = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.positiveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.negativeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of points";
            // 
            // NumberOfPoints
            // 
            this.NumberOfPoints.AutoSize = true;
            this.NumberOfPoints.Location = new System.Drawing.Point(120, 53);
            this.NumberOfPoints.Name = "NumberOfPoints";
            this.NumberOfPoints.Size = new System.Drawing.Size(0, 13);
            this.NumberOfPoints.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripButton,
            this.positiveToolStripButton,
            this.negativeToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(163, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // addToolStripButton
            // 
            this.addToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addToolStripButton.Image")));
            this.addToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addToolStripButton.Name = "addToolStripButton";
            this.addToolStripButton.Size = new System.Drawing.Size(61, 22);
            this.addToolStripButton.Text = "AddPoint";
            this.addToolStripButton.Click += new System.EventHandler(this.addToolStripButton_Click);
            // 
            // positiveToolStripButton
            // 
            this.positiveToolStripButton.CheckOnClick = true;
            this.positiveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.positiveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("positiveToolStripButton.Image")));
            this.positiveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.positiveToolStripButton.Name = "positiveToolStripButton";
            this.positiveToolStripButton.Size = new System.Drawing.Size(30, 22);
            this.positiveToolStripButton.Text = "x>0";
            this.positiveToolStripButton.Click += new System.EventHandler(this.positiveToolStripButton_Click);
            // 
            // negativeToolStripButton
            // 
            this.negativeToolStripButton.CheckOnClick = true;
            this.negativeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.negativeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("negativeToolStripButton.Image")));
            this.negativeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.negativeToolStripButton.Name = "negativeToolStripButton";
            this.negativeToolStripButton.Size = new System.Drawing.Size(30, 22);
            this.negativeToolStripButton.Text = "x<0";
            this.negativeToolStripButton.Click += new System.EventHandler(this.negativeToolStripButton_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(97, 26);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripButton_Click);
            // 
            // PointsCountView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(163, 97);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.NumberOfPoints);
            this.Controls.Add(this.label1);
            this.Name = "PointsCountView";
            this.Text = "PointsCountView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PointsCountView_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PointsCountView_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NumberOfPoints;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton addToolStripButton;
        private System.Windows.Forms.ToolStripButton positiveToolStripButton;
        private System.Windows.Forms.ToolStripButton negativeToolStripButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
    }
}