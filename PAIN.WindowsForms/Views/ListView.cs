﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace PAIN.WindowsForms
{
    public partial class ListView : Form
    {
        private bool positivesFilter = false;
        private bool negativesFilter = false;
        private int pointsShowed = 0;
        public ListView()
        {
            InitializeComponent();
        }
        void PointAddedEventHandler(Point point)
        {
            if(this.negativesFilter == true && point.x >= 0 || this.positivesFilter == true && point.x <= 0)
            {
                return;
            }
            ListViewItem item = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
            item.Tag = point;
            pointsList.Items.Add(item);
            this.pointsShowed++;
            this.pointsCount.Text = this.pointsShowed.ToString();
        }
        void PointDeletedEventHandler(Point point)
        {
            foreach (ListViewItem item in pointsList.Items)
            {
                if (item.Tag == point)
                {
                    this.pointsList.Items.Remove(item);
                    this.pointsShowed--;
                    this.pointsCount.Text = this.pointsShowed.ToString();
                    break;
                }
            }
        }
        void PointModifiedEventHandler(Point point)
        {
            for (int i = 0; i < pointsList.Items.Count; i++)
            {
                if (pointsList.Items[i].Tag == point)
                {
                    if (this.negativesFilter == true && point.x >= 0 || this.positivesFilter == true && point.x <= 0)
                    {
                        pointsList.Items[i].Remove();
                        this.pointsShowed--;
                    }
                    else
                    {
                        pointsList.Items[i] = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
                        pointsList.Items[i].Tag = point;
                    }
                    return;
                }
            }
            if (this.negativesFilter == true && point.x < 0 || this.positivesFilter == true && point.x > 0)
            {
                ListViewItem item = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
                item.Tag = point;
                pointsList.Items.Add(item);
                this.pointsShowed++;
                this.pointsCount.Text = this.pointsShowed.ToString();
            }
        }
        void ShowPoints()
        {
            pointsList.Items.Clear();
            foreach (Point point in ((MainWindow)MdiParent).points)
            {
                ListViewItem item = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
                item.Tag = point;
                pointsList.Items.Add(item);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            MainWindow mainWindow = (MainWindow)MdiParent;
            mainWindow.PointAdded += PointAddedEventHandler;
            mainWindow.PointDeleted += PointDeletedEventHandler;
            mainWindow.PointModified += PointModifiedEventHandler;
            this.pointsShowed = mainWindow.points.Count;
            this.pointsCount.Text = this.pointsShowed.ToString();
            ShowPoints();
        }

        protected override void OnActivated(EventArgs e)
        {
            ToolStripManager.Merge(this.toolStrip1, ((MainWindow)MdiParent).toolStrip1);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            ToolStripManager.Merge(((MainWindow)MdiParent).toolStrip1, this.toolStrip1);
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pointsList.SelectedItems.Count > 0)
                ((MainWindow)MdiParent).deletePoint((Point)this.pointsList.SelectedItems[0].Tag);
        }

        private void modifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pointsList.SelectedItems.Count > 0)
                ((MainWindow)MdiParent).modifyPoint((Point)this.pointsList.SelectedItems[0].Tag);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((MainWindow)MdiParent).addPoint();
        }

        private void ListView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!(e.CloseReason == CloseReason.MdiFormClosing))
            {
                if (((MainWindow)MdiParent).ViewsOpenedCount < 2)
                {
                    e.Cancel = true;
                    MessageBox.Show("At least one view must be opened");
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void ListView_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((MainWindow)MdiParent).ViewsOpenedCount--;
        }

        private void positiveToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.positiveToolStripButton.Checked)
            {
                positivesFilter = true;
                for (int i = 0; i < pointsList.Items.Count; i++)
                {
                    if (((Point)pointsList.Items[i].Tag).x <= 0)
                    {
                        pointsList.Items[i].Remove();
                        this.pointsShowed--;
                        i--;
                    }
                }
            }
            else
            {
                positivesFilter = false;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x < 0 || point.x == 0 && negativesFilter == false)
                    {
                        ListViewItem item = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
                        item.Tag = point;
                        this.pointsShowed++;
                        pointsList.Items.Add(item);
                    }
                }
            }
            this.pointsCount.Text = this.pointsShowed.ToString();
        }

        private void negativeToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.negativeToolStripButton.Checked)
            {
                negativesFilter = true;
                for (int i = 0; i < pointsList.Items.Count; i++)
                {
                    if (((Point)pointsList.Items[i].Tag).x >= 0)
                    {
                        pointsList.Items[i].Remove();
                        this.pointsShowed--;
                        i--;
                    }
                }
            }
            else
            {
                negativesFilter = false;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x > 0 || point.x == 0 && positivesFilter == false)
                    {
                        ListViewItem item = new ListViewItem(new[] { point.pointName, point.x.ToString(), point.y.ToString(), point.color.ToString() });
                        item.Tag = point;
                        this.pointsShowed++;
                        pointsList.Items.Add(item);
                    }
                }
            }
            this.pointsCount.Text = this.pointsShowed.ToString();
        }
    }
}
