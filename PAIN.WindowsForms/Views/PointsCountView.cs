﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAIN.WindowsForms
{
    public partial class PointsCountView : Form
    {
        private int numberOfPoints;
        private bool positivesFilter = false;
        private bool negativesFilter = false;
        public PointsCountView()
        {
            InitializeComponent();
        }
        void PointAddedEventHandler(Point point)
        {
            numberOfPoints++;
            this.NumberOfPoints.Text = numberOfPoints.ToString();
        }

        void PointDeletedEventHandler(Point point)
        {
            numberOfPoints--;
            this.NumberOfPoints.Text = numberOfPoints.ToString();
        }

        protected override void OnLoad(EventArgs e)
        {
            MainWindow mainWindow = (MainWindow)MdiParent;
            mainWindow.PointAdded += PointAddedEventHandler;
            mainWindow.PointDeleted += PointDeletedEventHandler;
            numberOfPoints = mainWindow.points.Count;
            this.NumberOfPoints.Text = numberOfPoints.ToString();
        }

        protected override void OnActivated(EventArgs e)
        {
            ToolStripManager.Merge(this.toolStrip1, ((MainWindow)MdiParent).toolStrip1);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            ToolStripManager.Merge(((MainWindow)MdiParent).toolStrip1, this.toolStrip1);
        }

        private void PointsCountView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (((MainWindow)MdiParent).ViewsOpenedCount < 2)
            {
                e.Cancel = true;
                MessageBox.Show("At least one view must be opened");
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void PointsCountView_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((MainWindow)MdiParent).ViewsOpenedCount--;
        }

        private void addToolStripButton_Click(object sender, EventArgs e)
        {
            ((MainWindow)MdiParent).addPoint();
        }

        private void positiveToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.positiveToolStripButton.Checked)
            {
                this.positivesFilter = true;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x < 0 || point.x == 0 && this.negativesFilter == false)
                        this.numberOfPoints--;
                }
            }
            else
            {
                this.positivesFilter = false;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x < 0 || point.x == 0 && this.negativesFilter == false)
                        this.numberOfPoints++;
                }
            }
            this.NumberOfPoints.Text = numberOfPoints.ToString();
        }

        private void negativeToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.negativeToolStripButton.Checked)
            {
                this.negativesFilter = true;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x > 0 || point.x == 0 && this.positivesFilter == false)
                        this.numberOfPoints--;
                }
            }
            else
            {
                this.negativesFilter = false;
                foreach (Point point in ((MainWindow)MdiParent).points)
                {
                    if (point.x > 0 || point.x == 0 && this.positivesFilter == false)
                        this.numberOfPoints++;
                }
            }
            this.NumberOfPoints.Text = numberOfPoints.ToString();
        }
    }
}
