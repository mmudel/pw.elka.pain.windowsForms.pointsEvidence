﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAIN.WindowsForms
{
    public class Point
    {
        public String pointName;
        public float x;
        public float y;
        public PointColor color;
        public Point(String pointName, float x, float y, PointColor color)
        {
            this.pointName = pointName;
            this.x = x;
            this.y = y;
            this.color = color;
        }
    }
}
