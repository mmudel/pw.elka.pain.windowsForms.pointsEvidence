﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAIN.WindowsForms
{
    public partial class PointColorControl : UserControl
    {
        private PointColor pointColor = PointColor.RED;
        [EditorAttribute(typeof(PointColorControlEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Color control")]
        [BrowsableAttribute(true)]
        public PointColor PointColor
        {
            get { return pointColor; }
            set { pointColor = value;
                this.Invalidate(false);
            }
        }

        public PointColorControl(PointColor color)
        {
            InitializeComponent();
            this.pointColor = color;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Color color = toColor();
            Rectangle rect = new Rectangle(0, 0, Size.Width, Size.Height);
            e.Graphics.DrawRectangle(new Pen(color, 0), rect);
            e.Graphics.FillRectangle(new SolidBrush(color), rect);
        }
        public void changeColor()
        {
            switch(this.pointColor)
            {
                case PointColor.RED:
                    PointColor = PointColor.BLUE;
                    break;
                case PointColor.BLUE:
                    this.pointColor = PointColor.GREEN;
                    break;
                case PointColor.GREEN:
                    this.pointColor = PointColor.BLACK;
                    break;
                case PointColor.BLACK:
                    this.pointColor = PointColor.RED;
                    break;
            }
            this.Invalidate(false);
        }

        public Color toColor()
        {
            Color color = Color.Red;
            switch (this.pointColor)
            {
                case PointColor.RED:
                    color = Color.Red;
                    break;
                case PointColor.GREEN:
                    color = Color.Green;
                    break;
                case PointColor.BLUE:
                    color = Color.Blue;
                    break;
                case PointColor.BLACK:
                    color = Color.Black;
                    break;
            }
            return color;
        }
        public PointColorControl()
        {
            InitializeComponent();
        }

        private void PointColorControl_Click(object sender, EventArgs e)
        {
            this.changeColor();
        }
    }
}
