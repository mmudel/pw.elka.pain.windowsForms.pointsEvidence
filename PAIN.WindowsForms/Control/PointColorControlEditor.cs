﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Design;

namespace PAIN.WindowsForms
{
    class PointColorControlEditor : System.Drawing.Design.UITypeEditor
    {
        public override void PaintValue(PaintValueEventArgs e)
        {
            Color color = (Color)e.Value;
            Rectangle rect = new Rectangle(0, 0, e.Bounds.X, e.Bounds.Y);
            e.Graphics.DrawRectangle(new Pen(color, 0), rect);
            e.Graphics.FillRectangle(new SolidBrush(color), rect);
        }

        public override bool GetPaintValueSupported(System.ComponentModel.ITypeDescriptorContext context)
        {
            return true;
        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc != null)
            {
                PointColorControl pointColorControl = new PointColorControl((PointColor)value);
                edSvc.DropDownControl(pointColorControl);
                return pointColorControl.PointColor;
            }
            return value;
        }
    }
}
