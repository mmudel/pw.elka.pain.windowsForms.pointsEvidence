﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAIN.WindowsForms
{
    public partial class AddPoint : Form
    {
        private String pointName;
        private float x;
        private float y;
        private PointColor color;
        private ErrorProvider errorProvider = new ErrorProvider();

        public String PointName
        {
            get { return pointName; }
            set { pointName = value; }
        }
        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public PointColor Color
        {
            get { return color; }
            set { color = value; }
        }


        public AddPoint()
        {
            InitializeComponent();
        }

        private void OK_button_Click(object sender, EventArgs e)
        {
            if (ValidateChildren())
            {
                this.pointName = nameTextBox.Text;
                this.x = float.Parse(xTextBox.Text);
                this.y = float.Parse(yTextBox.Text);
                this.color = this.pointColorControl1.PointColor;
                DialogResult = DialogResult.OK;
            }
        }

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void nameTextBox_Validating(object sender, CancelEventArgs e)
        {

        }

        private void xTextBox_Validating(object sender, CancelEventArgs e)
        {
            try { Convert.ToDouble(xTextBox.Text); }
            catch {
                e.Cancel = true;
                errorProvider.SetError(xTextBox, "Wprowadź liczbę.");
            }
        }

        private void yTextBox_Validating(object sender, CancelEventArgs e)
        {
            try { Convert.ToDouble(yTextBox.Text); }
            catch {
                e.Cancel = true;
                errorProvider.SetError(yTextBox, "Wprowadź liczbę.");
            }
        }

        private void yTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError(yTextBox, "");
        }

        private void xTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError(xTextBox, "");
        }

        private void pointColorControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
