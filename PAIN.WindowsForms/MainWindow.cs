﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace PAIN.WindowsForms
{
    public delegate void AddPointHandler(Point point);
    public delegate void DeletePointHandler(Point point);
    public delegate void ModifyPointHandler(Point point);
    public partial class MainWindow : Form
    {
        public event AddPointHandler PointAdded;
        public event DeletePointHandler PointDeleted;
        public event ModifyPointHandler PointModified;
        public ArrayList points;
        ListView listView;
        PointsCountView pointsCountView;
        private int viewsOpenedCount;

        public int ViewsOpenedCount
        {
            get { return viewsOpenedCount; }
            set { viewsOpenedCount = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
            points = new ArrayList();
            listView = new ListView();
            listView.MdiParent = this;
            this.viewsOpenedCount = 1;
            listView.Show();
        }

        public void deletePoint(Point point)
        {
            foreach (Point pointToDelete in points)
            {
                if (pointToDelete.pointName == point.pointName)
                {
                    this.points.Remove(pointToDelete);
                    if (PointDeleted != null)
                        PointDeleted(point);
                    break;
                }
            }
        }

        public void modifyPoint(Point point)
        {
            ModifyPoint modifyPoint = new ModifyPoint(point.pointName, point.x, point.y, point.color);
            if (modifyPoint.ShowDialog() == DialogResult.OK)
            {
                point.pointName = modifyPoint.PointName;
                point.x = modifyPoint.X;
                point.y = modifyPoint.Y;
                point.color = modifyPoint.Color;
                if (PointModified != null)
                    PointModified(point);
            }
        }

        public void addPoint()
        {
            AddPoint addPoint = new AddPoint();
            if (addPoint.ShowDialog() == DialogResult.OK)
            {
                Point newPoint = new Point(addPoint.PointName, addPoint.X, addPoint.Y, addPoint.Color);
                points.Add(newPoint);
                if (PointAdded != null)
                    PointAdded(newPoint);
            }
        }

        private void listViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView = new ListView();
            listView.MdiParent = this;
            this.viewsOpenedCount++;
            listView.Show();
        }
        private void numberOfPointsViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pointsCountView = new PointsCountView();
            pointsCountView.MdiParent = this;
            this.viewsOpenedCount++;
            pointsCountView.Show();
        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addPoint();
        }

    }
}
